package P3;

public interface Operation {
	public int operate(int a, int b);
}
