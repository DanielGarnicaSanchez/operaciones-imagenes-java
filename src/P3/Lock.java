package P3;

public interface Lock {
	
	public void requestCR( int pid );
	
	public void releaseCR( int pid );

}
