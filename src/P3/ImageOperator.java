package P3;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

public class ImageOperator extends Thread{
	private Image imgA;
	private Image imgB;
	public Image imgRes;
	private ImageData imgDataA;
	private ImageData imgDataB;
	private ImageData imgDataR;
	private RGB[][] img1;
	private RGB[][] img2;
	private RGB[][] imgR;
	private int chunkRows;
	private int chunkCols;
	volatile private int chunkCounter;
	private int ancho;
	private int altura;
	double alfa=0.0;
	volatile boolean[][] boolMatrix;
	int id;
	Lock lock;
	int opc;

	public ImageOperator(Image imgA, Image imgB, int ccols, int crows, int _id, Lock _lock) {
		this.chunkCols = ccols;
		this.chunkRows = crows;
		this.chunkCounter = ccols * crows; 
		this.imgA = imgA;
		this.imgB = imgB;
		this.imgDataA = this.imgA.getImageData();
		this.imgDataB = this.imgB.getImageData();
		this.img1 = new RGB[imgDataA.width][imgDataA.height];
		this.img2 = new RGB[imgDataB.width][imgDataB.height];
		
		ancho = Math.min(imgDataA.width, imgDataB.width);
		altura = Math.min(imgDataA.height,imgDataB.height);
		
		this.imgR = new RGB[ancho][altura];
		this.imgDataR = new ImageData(ancho, altura, imgDataA.depth, imgDataB.palette);	
		
		this.boolMatrix = new boolean[chunkCols][chunkRows];
		this.id = _id;
		this.lock = _lock;
		
		pixel2matrix();
		initMatrix();
	}
	
	public void initMatrix() {
		for(int i = 0; i < chunkCols; i++) {
			for(int j = 0; j < chunkRows; j++) {
				this.boolMatrix[i][j] = false;
			}
		}
	}
	
	public void pixel2matrix() {
		int pixelA, pixelB;
		for(int i = 0; i < imgDataA.width; i++) {
			for(int j = 0; j < imgDataA.height; j++) {
				pixelA = imgDataA.getPixel(i,j);
				//System.out.println("raw data A: " + imgDataA.data[i*j]);
				img1[i][j] = imgDataA.palette.getRGB(pixelA);
				//System.out.println("RGB data A: " + img1[i][j]);
			}
		}		
		for(int i = 0; i < imgDataB.width; i++) {
			for(int j = 0; j < imgDataB.height; j++) {
				pixelB = imgDataB.getPixel(i,j);
				//System.out.println("raw data B: " + imgDataB.data[i*j]);
				img2[i][j] = imgDataB.palette.getRGB(pixelB);
				//System.out.println("RGB data B: " + img2[i][j]);
			}
		}

	}
	
	public byte[] matrix2pixel() {
		byte[] res = new byte[ancho * altura];
		for(int i = 0; i < ancho; i++) {
			for(int j = 0; j < altura; j++) {
				res[i*j] = (byte) ((imgR[i][j].red << 16) | (imgR[i][j].green << 8) | imgR[i][j].blue );
				System.out.println("byte " + res[i*j]);
			}	

		}
		return res;
	}
	
	public Chunk obtainChunk(int x, int y) {
		int ccols, crows;
		int offsetx, offsety;
		ccols = (int) Math.ceil((double) ancho / chunkCols);
		crows = (int) Math.ceil((double) altura / chunkRows);
		offsetx = ccols;
		offsety = crows;
		if(x == chunkCols - 1 && (ancho % chunkCols) != 0) {
			ccols = ancho - (ccols*(chunkCols -1));

		}
		if(y == chunkRows - 1 && (altura % chunkRows) != 0) {
			crows = altura - (crows*(chunkRows -1));
		}
		return new Chunk(x * offsetx, y * offsety, crows, ccols);
	}
	
	public void procesar(int opc){
		int ancho = Math.min(imgDataA.width, imgDataB.width);
		int altura = Math.min(imgDataA.height,imgDataB.height);


		//System.out.println("Dimensiones: " + ancho + " x " + altura);	
		Operation operation;
		switch(opc) 
		{
		case 1:
			operation = this::sumar;
			break;
		case 2:
			operation = this::restar;
			break;
		case 3:
			operation = this::multiplicar;
			break;
		case 4:
			operation = this::cLineal;
			break;
		default:
			operation = this::sumar;
			break;
		}
		
		for(int i = 0; i < chunkCols; i++) {
			for(int j = 0; j < chunkRows; j++){
				doOperation(obtainChunk(i, j), operation);
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {					
					e.printStackTrace();
				}
			}
		}
		
		this.imgRes = new Image(Display.getDefault(), imgDataR );

		System.out.println("Fin de Procesamiento");
//		ImageLoader saver = new ImageLoader();
//		saver.data = new ImageData[] { imgRes.getImageData() };
//		saver.save("resultado.png", SWT.IMAGE_PNG);

		ImageLoader imageLoader = new ImageLoader();
		imageLoader.data = new ImageData[] {imgRes.getImageData()};
		imageLoader.save("resultado.jpg",SWT.IMAGE_JPEG);

	}
	
	public void setAlfa(double a){
		this.alfa=a;
	}
	
	public int sumar(int c1, int c2) {
		int suma=255;
		if((c1 + c2) <= 255) {
			suma = c1 + c2;
		}
		return suma;
	}
	
	private ImageData doOperation(Chunk chunk, Operation operation) {
		int pixelSuma = 0;
		int pixelA, pixelB;

		int r,g,b;
		
		for(int i = chunk.cpos.x; i < (chunk.cpos.x + chunk.ccols); i++) {
			for(int j = chunk.cpos.y; j < (chunk.cpos.y + chunk.crows); j++) {			
				r = operation.operate(img1[i][j].red,   img2[i][j].red);
				g = operation.operate(img1[i][j].green, img2[i][j].green);
				b = operation.operate(img1[i][j].blue,  img2[i][j].blue);
				pixelSuma = (r << 16) | (g << 8) | b;
				imgDataR.setPixel(i, j, pixelSuma);
			}
		}		
		
		return imgDataR;

	}
	
	public int restar(int c1, int c2) {
		int resta = 0;
		if((c1 - c2) >= 0) {
			resta = c1 - c2;
		}
		return resta;
	}

	public int multiplicar(int c1, int c2) {
		int multi = 0;
		multi = (c1*c2)/255;
		return multi;
	}

	public int cLineal(int c1, int c2) {
		double beta = (double)1.0-alfa;
		double comb = 0;

		comb = (alfa*c1) + (beta*c2);
		return (int) comb;
	}

	public void run() {
		while(chunkCounter > 0) {
			lock.requestCR( id );
			CriticalRegion();
			
			lock.releaseCR( id );
			nonCriticalRegion();
		}
	}

	private void CriticalRegion() {
		//System.out.println("Estoy en la region Critica, ID: " + this.id);
		procesar(this.opc);
		chunkCounter--;
	}

	private void nonCriticalRegion() {
		//System.out.println("NO estoy en la region Critica, ID: " + this.id);
	}
	
	public void setOpc(int _opc) {
		this.opc = _opc;
	}
}
