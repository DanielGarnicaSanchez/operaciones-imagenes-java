package Img.gui;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import P3.ImageOperator;
import P3.Lock;
import P3.Bakery;

import org.eclipse.swt.widgets.Label;

import java.awt.image.BufferedImage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.widgets.Slider;


public class MainWindow {

	protected Shell shell;
	
	String[] filtro = { "*.jpg", "*.png"};
	Image imageA = new Image(Display.getDefault(), 1, 1);
	Image imageB = new Image(Display.getDefault(), 1, 1);
	Image imageResult = new Image(Display.getDefault(), 1, 1);
	BufferedImage img;
	int chunkFila = 1;
	int chunkCol = 1;
	double alfa = 0;
	int N = 1;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(690, 719);
		shell.setText("SWT Application");
		
		Label lblImg2 = new Label(shell, SWT.NONE);
		lblImg2.setAlignment(SWT.CENTER);
		lblImg2.setBounds(414, 10, 250, 250);
		lblImg2.setText("img2");
		
		Label lblImg = new Label(shell, SWT.NONE);
		lblImg.setAlignment(SWT.CENTER);
		lblImg.setBounds(10, 10, 250, 250);
		lblImg.setText("img1");
		
		Label lblImgres = new Label(shell, SWT.NONE);
		lblImgres.setAlignment(SWT.CENTER);
		lblImgres.setText("imgRes");
		lblImgres.setBounds(221, 391, 250, 250);
		
		Combo combo = new Combo(shell, SWT.READ_ONLY);
		combo.setItems(new String[] {"+", "-", "*", "#"});
		combo.setBounds(292, 66, 91, 23);
		combo.select(0);
		
		Label lblCombinacinLineal = new Label(shell, SWT.NONE);
		lblCombinacinLineal.setAlignment(SWT.CENTER);
		lblCombinacinLineal.setBounds(266, 110, 142, 25);
		lblCombinacinLineal.setText("Combinaci\u00F3n Lineal");
		
		Button btnOperar = new Button(shell, SWT.NONE);
		btnOperar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Lock lock = new Bakery(N);
				ImageOperator[] t = new ImageOperator[N];
				
				for( int i = 0; i < N; i++ ) {
					t[i] = new ImageOperator(imageA, imageB, chunkFila, chunkCol, i, lock);
					int opc = combo.getSelectionIndex();
					t[i].setAlfa(alfa);
					t[i].setOpc(opc+1);
					
					t[i].start();
					
					Image imageResult = new Image(Display.getCurrent(), "resultado.jpg");
			        ImageData data = imageResult.getImageData();
			        
			        int tamMin=Math.min(data.height,data.width);
			        int tamMax=Math.max(data.height,data.width);
			        
			        double norm=((((double)tamMin)/((double)tamMax)))*((double)250);
		
			        if(data.height>data.width)
			        	data = imageResult.getImageData().scaledTo((int)norm, 250);
			        else
			        	if(data.height<data.width)
			        		data = imageResult.getImageData().scaledTo(250, (int)norm);
			        	else if (data.height==data.width)
			        		data = imageResult.getImageData().scaledTo(250, 250);
			        		
			        Image imageScaled = new Image(Display.getDefault(), data);
			        lblImgres.setImage(imageScaled);
				}
			}
		});
		btnOperar.setBounds(292, 35, 91, 25);
		btnOperar.setText("Operar");
		
		Button btnCargarImg = new Button(shell, SWT.NONE);
		btnCargarImg.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				//Selecciona ImgA
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setText("Seleccionar Img");
		        fd.setFilterPath("C:/");
		        fd.setFilterExtensions(filtro);
		        String selected = fd.open();
		        imageA = new Image(Display.getCurrent(), selected);
		        ImageData data = imageA.getImageData();
		        int tamMin=Math.min(data.height,data.width);
		        int tamMax=Math.max(data.height,data.width);
		        double norm=((((double)tamMin)/((double)tamMax)))*((double)250);
	
		        if(data.height>data.width)
		        	data = imageA.getImageData().scaledTo((int)norm, 250);
		        else
		        	if(data.height<data.width)
		        		data = imageA.getImageData().scaledTo(250, (int)norm);
		        	else if (data.height==data.width)
		        		data = imageA.getImageData().scaledTo(250, 250);
		        		
		        Image imageScaled = new Image(Display.getDefault(), data);
		        lblImg.setImage(imageScaled);
		        
		        
			}
		});
		btnCargarImg.setBounds(46, 274, 176, 25);
		btnCargarImg.setText("Cargar Imagen A");
		
		Button btnCargarImg2 = new Button(shell, SWT.NONE);
		btnCargarImg2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				// Selecciona ImgB
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setText("Seleccionar Img");
		        fd.setFilterPath("C:/");
		        fd.setFilterExtensions(filtro);
		        String selected = fd.open();
		        imageB = new Image(Display.getCurrent(), selected);
		        ImageData data = imageB.getImageData();
		        int tamMin=Math.min(data.height,data.width);
		        int tamMax=Math.max(data.height,data.width);
		        double norm=((((double)tamMin)/((double)tamMax)))*((double)250);
		       
		        if(data.height>data.width)
		        	data = data.scaledTo((int)norm, 250);
		        else
		        	if(data.height<data.width)
		        		data = data.scaledTo(250, (int)norm);
		        	else if (data.height==data.width)
		        		data = data.scaledTo(250, 250);
		        		
		        Image imageScaled = new Image(Display.getDefault(), data);
		        lblImg2.setImage(imageScaled);
			}
		});
		btnCargarImg2.setText("Cargar Imagen B");
		btnCargarImg2.setBounds(456, 274, 176, 25);
		
		Spinner spinnerCol = new Spinner(shell, SWT.BORDER);
		spinnerCol.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				chunkCol = spinnerCol.getSelection();
			}
		});
		spinnerCol.setBounds(358, 226, 59, 26);
		spinnerCol.setSelection(1);
		spinnerCol.setMinimum(1);
		
		Spinner spinnerFila = new Spinner(shell, SWT.BORDER);
		spinnerFila.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				chunkFila = spinnerFila.getSelection();
			}
		});
		spinnerFila.setBounds(266, 226, 59, 26);
		spinnerFila.setSelection(1);
		spinnerFila.setMinimum(1);
		
		Label lblChunks = new Label(shell, SWT.NONE);
		lblChunks.setBounds(311, 200, 51, 20);
		lblChunks.setText("Chunks");
		
		Label lblX = new Label(shell, SWT.NONE);
		lblX.setBounds(337, 229, 12, 20);
		lblX.setText("x");
		
		Slider slider = new Slider(shell, SWT.NONE);
		slider.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				alfa = (Double.valueOf(slider.getSelection())  ) / 100;
				lblCombinacinLineal.setText(String.valueOf(alfa));
			}
		});
		slider.setBounds(266, 141, 142, 17);
		

		
		Label label = new Label(shell, SWT.NONE);
		label.setBounds(396, 169, 12, 28);
		label.setText("1");
		
		Label label_1 = new Label(shell, SWT.NONE);
		label_1.setText("0");
		label_1.setBounds(266, 169, 12, 28);
		

		
		Spinner spinnerHilos = new Spinner(shell, SWT.BORDER);
		spinnerHilos.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				N = spinnerHilos.getSelection();
			}
		});
		spinnerHilos.setSelection(1);
		spinnerHilos.setBounds(311, 317, 59, 26);
		spinnerHilos.setMinimum(1);
		
		Label lblNumHilos = new Label(shell, SWT.NONE);
		lblNumHilos.setText("Num Hilos:");
		lblNumHilos.setBounds(303, 291, 80, 20);
	}
	
}
