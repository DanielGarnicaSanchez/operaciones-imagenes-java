package Img.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class Dialogo extends Dialog {
		Label lblImg;
	  public Dialogo(Shell parent) {
	    super(parent);
	  }

	  public String open() {
	    Shell parent = getParent();
	    Shell dialog = new Shell(parent, SWT.DIALOG_TRIM
	        | SWT.APPLICATION_MODAL);
	    
	    dialog.setSize(400, 400);
	    dialog.setText("Resultado");
	    
	    Image imageC = new Image(Display.getCurrent(), "resultado.png");
	    Label lblImg = new Label(dialog, SWT.NONE);
	    
		lblImg.setBounds(0, 0, 400, 400);
		lblImg.setText("img");
		
		ImageData data = imageC.getImageData();
		
        int tamMin=Math.min(data.height,data.width);
        int tamMax=Math.max(data.height,data.width);
        
        double norm=((((double)tamMin)/((double)tamMax)))*((double)400);
       
        //Normalización de Imágenes
        if(data.height>data.width)
        	data = data.scaledTo((int)norm, 400);
        else
        	if(data.height<data.width)
        		data = data.scaledTo(400, (int)norm);
        	else if (data.height==data.width)
        		data = data.scaledTo(400, 400);
        		
        Image imageScaled = new Image(Display.getDefault(), data);
        lblImg.setImage(imageScaled);
	    dialog.open();
	    
	    Display display = parent.getDisplay();
	    
	    while (!dialog.isDisposed()) {
	      if (!display.readAndDispatch())
	        display.sleep();
	    }
	    return "After Dialog";
	  }
	}
